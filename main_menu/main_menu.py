#from game import Game
import pygame
import os
from escena import Escena
from selection import Seleccion

#logo = pygame.image.load(os.path.join("game_assets", "logo.png")).convert_alpha()


def divide_imagen(image, rect):
    subimage = image.subsurface(rect)
    return subimage, rect



class MainMenu(Escena):
    def __init__(self, director):
        Escena.__init__(self, director)
        self.width=director.win.get_width()
        self.height=director.win.get_height()

        self.titulo = pygame.image.load(os.path.join("Assets/main_menu", "titulo.png")).convert_alpha()
        self.pos_titulo = (103,30)
        self.btn_creditos = pygame.image.load(os.path.join("Assets/main_menu", "btn_creditos.png")).convert_alpha()
        self.btn_nova_partida = pygame.image.load(os.path.join("Assets/main_menu", "btn_nova_partida.png")).convert_alpha()
        self.btn_sair = pygame.image.load(os.path.join("Assets/main_menu", "btn_sair.png")).convert_alpha()
        self.pos_btn_creditos = (552,588)
        self.pos_btn_nova_partida = (80,588)
        self.pos_btn_sair = (1046,588)

        self.btn_hover = pygame.image.load(os.path.join("Assets/main_menu", "hover_final.png")).convert_alpha()

        self.cursor_pos = (0,0)

        self.hover = 0 # 1 : nova partida // 2 : creditos // 3: sair
        self.limites = (30,80) # limites onde oscila o titulo
        self.desplazamento = 0
        self.sentido = 1
        self.contador_tempo = 0
        self.tiras = []
        for x in range(0, self.titulo.get_height()):
            subimage, rect = divide_imagen(self.titulo, pygame.Rect((0,x) ,(self.titulo.get_width(),1) ))
            self.tiras.append(subimage)



    def update(self, tiempo):
        self.cursor_pos = pygame.mouse.get_pos()
        if self.cursor_pos[0] > self.pos_btn_nova_partida[0] and self.cursor_pos[1] > self.pos_btn_nova_partida[1] and \
            self.cursor_pos[0] < (self.pos_btn_nova_partida[0] + self.btn_nova_partida.get_width()) and \
                self.cursor_pos[1] < (self.pos_btn_nova_partida[1] + self.btn_nova_partida.get_height()):
            self.hover = 1
        
        elif self.cursor_pos[0] > self.pos_btn_creditos[0] and self.cursor_pos[1] > self.pos_btn_creditos[1] and \
            self.cursor_pos[0] < (self.pos_btn_creditos[0] + self.btn_creditos.get_width()) and \
                self.cursor_pos[1] < (self.pos_btn_creditos[1] + self.btn_creditos.get_height()):
            self.hover = 2

        elif self.cursor_pos[0] > self.pos_btn_sair[0] and self.cursor_pos[1] > self.pos_btn_sair[1] and \
            self.cursor_pos[0] < (self.pos_btn_sair[0] + self.btn_sair.get_width()) and \
                self.cursor_pos[1] < (self.pos_btn_sair[1] + self.btn_sair.get_height()):
            self.hover = 3    
        
        else:
            self.hover = 0



    def eventos(self, lista_eventos):
        #run = True
        #while run:
            for event in lista_eventos:
                self.cursor_pos = pygame.mouse.get_pos()
                if event.type == pygame.QUIT:
                    self.director.salirPrograma()

                if event.type == pygame.MOUSEBUTTONUP:
                    x, y = pygame.mouse.get_pos()

                    #if (self.btn[0] <= x <= self.btn[0] + self.btn[2]) and (self.btn[1] <= y <= self.btn[1] + self.btn[3]):
                    if self.hover == 1:
                        #from dialogs.dialog_scene import Dialogos
                        from dialogs.intro import Intro
                        #from dialogs.creditos import Creditos
                        #dialogo=[" ", " "] #Esta e a introduccion a aventura, querido companheiro  #Asi e, permite que me presente eu son: o prota
                        self.director.cambiarEscena(Intro(self.director))
                    
                    elif self.hover == 2:
                        from dialogs.creditos import Creditos
                        self.director.cambiarEscena(Creditos(self.director))

                    elif self.hover == 3:
                        self.director.salirPrograma()
                        #self.director.cambiarEscena(Dialogos(self.director, None, dialogo, "o", 8))
                        self.director.cambiarEscena(Creditos(self.director))

            self.dibujar(self.director.win)



    def dibujar(self, win):
        win.fill((0,0,0))
        
        win.blit(self.btn_nova_partida, self.pos_btn_nova_partida)
        win.blit(self.btn_creditos, self.pos_btn_creditos)
        win.blit(self.btn_sair, self.pos_btn_sair)

        self.contador_tempo += 1
        if self.contador_tempo == 6:
            self.desplazamento += self.sentido
            if self.desplazamento >= self.limites[1]:
                self.sentido = -1
            elif self.desplazamento <= self.limites[0]:
                self.sentido = 1
            self.contador_tempo = 0

        for i in range(len(self.tiras)):
            win.blit(self.tiras[i], (self.pos_titulo[0], self.pos_titulo[1] + i + self.desplazamento) )

        if self.hover == 1:
            win.blit(self.btn_hover, (self.pos_btn_nova_partida[0]-40, self.pos_btn_nova_partida[1]-40) )
        elif self.hover == 2:
            win.blit(self.btn_hover, (self.pos_btn_creditos[0]-80, self.pos_btn_creditos[1]-40) )
        elif self.hover == 3:
            win.blit(self.btn_hover, (self.pos_btn_sair[0]-130, self.pos_btn_sair[1]-40) )

        #win.blit(self.bg, (0,0))
        #if self.cursor_on_start_btn(self.cursor_pos):
        #    win.blit(self.start_btn_hover, (self.btn[0], self.btn[1]))
        #else:
        #    win.blit(self.start_btn, (self.btn[0], self.btn[1]))
        #pygame.display.update()
