import pygame
from escena import Escena
import os
from dialogs.dialog_scene import Dialogos

class Seleccion(Escena):
	def __init__(self, director, nivel):
		Escena.__init__(self, director)
		w=director.win.get_width()
		h=director.win.get_height()
		self.fondo= pygame.transform.scale(pygame.image.load(os.path.join("Assets","seleccion de personaje.png")),(w, h)).convert_alpha()
		self.nivel = nivel

		# Dialogos
		self.dialogo1 = ["Este e o dialogo previo ao primeiro nivel (post-seleccion)", "Oh, grazas pola informacion"]
		self.dialogo2 = ["Este e o dialogo previo ao segundo nivel (post-seleccion)", "Realmente e o meu nivel favorito"]
		self.dialogo3 = ["Este e o dialogo previo ao terceiro nivel (post-seleccion)", "Si, amigo, o final do xogo esta cerca!"]

	def update(self, *args):
		pass
	
	def eventos(self, lista_eventos):
		for event in lista_eventos:
			if event.type == pygame.QUIT:
				self.director.salirPrograma()

			if event.type == pygame.MOUSEBUTTONUP:
				# miramos las coordenadas para saber que personaje se ha elegido
				#rock (45,15, 1255, 205) paper (45, 245, 1255, 205) scissors (45, 475, 1255, 205)
				x, y = pygame.mouse.get_pos()
				dialogo=["Detén os teus ataques. A xente deste refuxio non quere as túas melloras.", "Por que? O máis precioso é o cerebro humano e, aínda así, permitimos que morra.", 
				"Todos sabemos o que implica, a resposta é non, se tratas de atacar, defenderémonos.", 
				"Melloras de metal e un corpo que nunca vai envellecer, como podes didir que non a iso? Ao seguinte nivel da humanidade.", 
				"O seguinte nivel da humanidade? Dirás someterse a túa voluntade.", "Moi ben, resistídevos... Que comence o ataque."]
				if self.nivel == 1:
					dialogo = self.dialogo1
				elif self.nivel == 2:
					dialogo = self.dialogo2 
				elif self.nivel == 3:
					dialogo = self.dialogo3
				#dialogo=["Este e o dialogo previo ao primeiro nivel (post-seleccion)", "Oh, grazas pola informacion"]
				#dialogo2 = ["Este e o dialogo previo ao segundo nivel (post-seleccion)", "Realmente e o meu nivel favorito"]
				if (45 <= x <= 45+1255) and (15 <= y <= 15+205): #rock
					#print("rock")
					escena = Dialogos(self.director, "rock", dialogo, "c", self.nivel)
					self.director.cambiarEscena(escena)
				elif (45 <= x <= 45+1255) and (245 <= y <= 245+205): #paper
					#print("paper")
					escena = Dialogos(self.director, "paper", dialogo,"c",self.nivel)
					self.director.cambiarEscena(escena)
				elif (45 <= x <= 45+1255) and (475 <= y <= 475+205): #scissors
					#print("scissors")
					escena = Dialogos(self.director, "scissors", dialogo,"c",self.nivel)
					self.director.cambiarEscena(escena)
	
	def dibujar(self, win):
		win.blit(self.fondo, (0,0))
		