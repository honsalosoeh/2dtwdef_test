import pygame
from escena import Escena
from director import Director
from main_menu.main_menu import MainMenu
from game import Game
pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.mixer.set_num_channels(4096)

if __name__ == "__main__":
    pygame.init()

    #crear director
    director=Director()
    #crear escena inicial, apilar y ejecutar
    escena=MainMenu(director)
    #escena1=Game(director)
    director.set_menu(escena)
    director.apilarEscena(escena)
    director.ejecutar()
    
    pygame.quit()
