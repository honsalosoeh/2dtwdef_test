import pygame
import os
from escena import Escena
#from dialogs.dialog_scene import Dialogos
#from selection import Seleccion

WHITE = (255, 255, 255)
MATTE_BLACK = (20, 20, 20)
COR_TEXTO = (191, 228, 230)
coord_panel = (470,60)
coord_titulo = (135,50)
coord_continuar = (195,200)
coord_reintentar = (180,200)
coord_voltar = (195, 400)
# Coords de hovers
coord_hover_continuar = (149,190)
coord_hover_reintentar = (165,210)
coord_hover_voltar = (132,390)
FONT_PIXEL = 'Assets/fonts/KnoxFont.ttf'



class EndLevelScreen(Escena):
    def __init__(self, director, comander, nivel, victory):
        Escena.__init__(self, director)
        self.director=director
        self.victory = victory
        self.window = pygame.display.get_surface()
        self.width, self.height = self.window.get_size()

        self.init_bg = 0
        self.background = pygame.Surface((self.width, self.height), pygame.SRCALPHA, 32)
        self.background.fill((*MATTE_BLACK, 160))

        self.panel = pygame.image.load(os.path.join("Assets/pantallas", "pantalla_final_nivel.png")).convert_alpha()
        self.victoria_titulo = pygame.image.load(os.path.join("Assets/pantallas", "victoria_titulo.png")).convert_alpha()
        self.derrota_titulo = pygame.image.load(os.path.join("Assets/pantallas", "derrota_titulo.png")).convert_alpha()
        self.hover_continuar = pygame.image.load(os.path.join("Assets/pantallas", "hover_continuar.png")).convert_alpha()
        self.hover_reintentar = pygame.image.load(os.path.join("Assets/pantallas", "hover_reintentar.png")).convert_alpha()
        self.hover_menu = pygame.image.load(os.path.join("Assets/pantallas", "hover_voltar.png")).convert_alpha()
        self.PIXEL_TEXT = pygame.font.Font(FONT_PIXEL, 11)

        self.cursor_pos = (0,0) # Para pillar os hovers
        self.hover = 0 # 0: non hai hover, 1: hai hover no primeiro btn, 2: hai hover no segundo btn

        self.comander=comander
        self.nivel = nivel


    def update(self, tiempo):
        self.cursor_pos = pygame.mouse.get_pos()
        if self.cursor_pos[0] > (coord_continuar[0]+coord_panel[0]-60) and self.cursor_pos[0] < (coord_continuar[0]+coord_panel[0]+180) and\
            self.cursor_pos[1] > (coord_continuar[1]+coord_panel[1]-30) and self.cursor_pos[1] < (coord_continuar[1]+coord_panel[1]+70):
            self.hover = 1
        elif self.cursor_pos[0] > (coord_voltar[0]+coord_panel[0]-60) and self.cursor_pos[0] < (coord_voltar[0]+coord_panel[0]+180) and\
            self.cursor_pos[1] > (coord_voltar[1]+coord_panel[1]-30) and self.cursor_pos[1] < (coord_voltar[1]+coord_panel[1]+70):
            self.hover = 2
        else:
            self.hover = 0
	
    def eventos(self, lista_eventos):
        for event in lista_eventos:
            self.cursor_pos = pygame.mouse.get_pos()
            if event.type == pygame.QUIT:
                self.director.salirPrograma()

            if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                from main_menu.main_menu import MainMenu
                from dialogs.dialog_scene import Dialogos
                from levels.level import Level1, Level2, Level3
                dialogo=["Tes que estar de broma....","Os nosos comandantes son bos no seu traballo. Cesa os teus ataques, non tes ningunha posibilidade.",
                "Teño soldados esperando nos sete continentes. Pensas que me vou rendir asi de facil. Non tes ningún xeito de pararme. Teño un exército. Unha especie propia.",
                "Unha especie metálica, unha sociedade metálica con persoas metálicas e pensamentos metálicos, sen o único que fai que este planeta estea tan vivo. Xente. Xente común, estúpida, brillante.",
                "A persoas presentan fallos, enferman, pelexan entre eles.... Coa miña proposta todo iso queda no pasado.",
                "Todo o que inventaches, fixechelo para mellorar a vida das persoas. Iso é brillante. Moi humano. Podeste librar das enfermidades e do envellecemento sen converter ás persoas en máquinas ao teu control.",
                "Moi ben entón, vexo que non queres rendirte, prepara aos teus comandantes esta vez non será tan doado."
                ]
                dialogo=["Este e o dialogo previo ao segundo nivel (pre-seleccion)", "Asi e, podo confirmar"]
                dialogo2=["Este e o dialogo previo ao terceiro nivel (pre-seleccion)", "Correcto meu estimado amigo Chema"]
                dialogo3=["Este e o dialogo final do xogo", "Foi un pracer loitar xunto a tan estimado oponente"]
                if self.hover == 1 and self.victory:
                    #self.director.cambiarEscena(self.director.dialogo)
                    if self.nivel == 1:
                        self.director.cambiarEscena(Dialogos(self.director, self.comander, dialogo, "o", 10))
                    elif self.nivel == 2:
                        self.director.cambiarEscena(Dialogos(self.director, self.comander, dialogo2, "o", 11))
                    elif self.nivel == 3:
                        self.director.cambiarEscena(Dialogos(self.director, self.comander, dialogo3, "o", 4))
                    print("Transicion ao seguinte nivel (Escena de dialogo)")
                elif self.hover == 1 and not self.victory:
                    self.director.cambiarEscena(Level1(self.director, self.comander))
                    #self.director.cambiarEscena(self.director.level1)
                    print("Transicion ao mesmo nivel (Escena LevelX)")
                elif self.hover == 2:
                    #self.director.cambiarEscena(self.director.main_menu)
                    self.director.cambiarEscena(MainMenu(self.director))
                    print("Transicion a o menu principal (Escena Menu principal)")
                #self.director.cambiarEscena(Seleccion(self.director))



    def dibujar(self, window):
        if not self.init_bg:
            self.end_level_setup(window)
            self.init_bg = 1
        window.blit(self.background, (0,0))
        window.blit(self.panel, coord_panel)

        # Se ganhache debuxa titulo de victoria e boton de continuar, senon o de reintentar
        if self.victory:
            window.blit(self.victoria_titulo, (coord_panel[0]+coord_titulo[0], coord_panel[1]+coord_titulo[1]) )
            self.draw_text("CONTINUAR", self.PIXEL_TEXT, COR_TEXTO, (coord_panel[0]+coord_continuar[0], coord_panel[1]+coord_continuar[1]), self.window)   
        else:
            window.blit(self.derrota_titulo, (coord_panel[0]+coord_titulo[0], coord_panel[1]+coord_titulo[1]) )
            self.draw_text("REINTENTAR", self.PIXEL_TEXT, COR_TEXTO, (coord_panel[0]+coord_continuar[0], coord_panel[1]+coord_continuar[1]), self.window)

        self.draw_text("VOLTAR AO MENU", self.PIXEL_TEXT, COR_TEXTO, (coord_panel[0]+coord_voltar[0], coord_panel[1]+coord_voltar[1]), self.window)

        if self.hover == 1:
            window.blit(self.hover_continuar, (coord_hover_continuar[0] + coord_panel[0], coord_hover_continuar[1] + coord_panel[1]) )
        elif self.hover == 2:
            window.blit(self.hover_menu, (coord_hover_voltar[0] + coord_panel[0], coord_hover_voltar[1] + coord_panel[1]) )


    def draw_text(self, text, font, COLOUR, POS, window):
        text_surf = font.render(text, True, COLOUR)
        text_rect = text_surf.get_rect()
        text_rect.center = POS
        window.blit(text_surf, text_rect)
    
    def end_level_setup(self, window):
        window.blit(self.background, (0,0))
        pygame.display.update()
        self.background = window.copy()
