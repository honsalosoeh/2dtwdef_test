import pygame
import os
from escena import Escena



COR_TEXTO = (191,228,230)

def divide_imagen(image, rect):
    subimage = image.subsurface(rect)
    return subimage, rect



def draw_text(text, font, COLOUR, POS, window):
    text_surf = font.render(text, True, COLOUR)
    text_rect = text_surf.get_rect()
    text_rect.center = POS
    window.blit(text_surf, text_rect)


class Intro(Escena):
    def __init__(self, director):
        self.director=director
        self.tam_intro = (544, 320)

        self.intros = []
        self.intros.append(pygame.transform.scale(pygame.image.load(os.path.join("Assets/intro/","intro1.png")).convert_alpha(), self.tam_intro) )
        self.intros.append(pygame.transform.scale(pygame.image.load(os.path.join("Assets/intro/","intro2.png")).convert_alpha(), self.tam_intro) )
        self.intros.append(pygame.transform.scale(pygame.image.load(os.path.join("Assets/intro/","intro3.png")).convert_alpha(), self.tam_intro) )
        self.intros.append(pygame.transform.scale(pygame.image.load(os.path.join("Assets/intro/","intro4.png")).convert_alpha(), self.tam_intro) )

        self.pos_bg = (380,50)
        self.pos_texto = (200, 505)
        self.tiras_total = []
        self.tiras_intro1 = []
        self.tiras_intro2 = []
        self.tiras_intro3 = []
        self.tiras_intro4 = []
        
        self.current_image = 0
        self.counter_transicion = 0
        self.debuxar_texto_agora = False

        FONT_PIXEL = 'Assets/fonts/KnoxFont.ttf'
        self.PIXEL_TEXT = pygame.font.Font(FONT_PIXEL, 16)
        self.textos = []
        self.textos.append("Fai xa moitos anos unha gran compañia industrial desarrollou un produto para terminar coas enfermidades e as debilidades dos humans")
        self.textos.append("A sua influencia espallouse por todo o mundo pero as suas intencions estaban corruptas")
        self.textos.append("Ansiaron crear unha nova especie, conducir a humanidade a unha nova era, pero esto so trouxo a maior das guerras")
        self.textos.append("Despois de moitos anos de loita, a resistencia quedou debilitada, a compañia dirixe agora a sua atencion cara un dos pilares da resistencia,\
 o bastion Fort Knox comandado polo lexendario excombatente Chema Jenkins")


        #self.chema_falar = pygame.image.load(os.path.join("Assets/dialogos","chema_falar.png")).convert_alpha()

        for x in range(0, self.intros[0].get_width()):
            subimage, rect = divide_imagen(self.intros[0], pygame.Rect((x,0) ,(1,self.tam_intro[1]) ))
            self.tiras_intro1.append(subimage)
        for x in range(0, self.intros[1].get_width()):
            subimage, rect = divide_imagen(self.intros[1], pygame.Rect((x,0) ,(1,self.tam_intro[1]) ))
            self.tiras_intro2.append(subimage)
        for x in range(0, self.intros[2].get_width()):
            subimage, rect = divide_imagen(self.intros[2], pygame.Rect((x,0) ,(1,self.tam_intro[1]) ))
            self.tiras_intro3.append(subimage)
        for x in range(0, self.intros[3].get_width()):
            subimage, rect = divide_imagen(self.intros[3], pygame.Rect((x,0) ,(1,self.tam_intro[1]) ))
            self.tiras_intro4.append(subimage)
        self.tiras_total.append(self.tiras_intro1)
        self.tiras_total.append(self.tiras_intro2)
        self.tiras_total.append(self.tiras_intro3)
        self.tiras_total.append(self.tiras_intro4)


        # VARIABLES DE IVAN
        self.num=0
        self.aux=""
        self.i=0


    
    def update(self, *args):
        pass

        
        
        
    def eventos(self, lista_eventos):
        for event in lista_eventos:
            if event.type == pygame.QUIT:
                self.director.salirPrograma()
            if self.num== len(self.textos)-1 and event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                from dialogs.dialog_scene import Dialogos
                dialogo = ["Esto seria o dialogo do nivel 1 (pre-seleccion)", "Con total seguridade"]
                escena = Dialogos(self.director, None, dialogo, "o", 9)
                self.director.cambiarEscena(escena)
            if event.type == pygame.MOUSEBUTTONUP and event.button == 1:# and self.finDialogo==True:
                self.num+=1
                self.i=0
                self.aux=""
                self.finDialogo=False
                self.debuxar_texto_agora = False
                self.counter_transicion = 0
                if self.current_image < len(self.intros)-1:
                    self.current_image += 1
        
        
        
    def dibujar(self, win):
        win.fill((0,0,0))
        self.counter_transicion += 3
        if self.counter_transicion >= self.tam_intro[0]:
            #self.counter_transicion = 0
            #self.current_image += 1
            win.blit(self.intros[self.current_image], self.pos_bg)
            self.debuxar_texto_agora = True
            #draw_text("CHATARRA : " + str(scrap), self.PIXEL_TEXT, (44,54,52), (width-170, 32), window)
            #draw_text(self.textos[self.current_image], self.PIXEL_TEXT, COR_TEXTO, self.pos_texto, win)
        else:
            for i in range(self.counter_transicion):
                win.blit(self.tiras_total[self.current_image][i], (self.pos_bg[0]+i, self.pos_bg[1]))


        if self.debuxar_texto_agora:
            # COUSA DE IVAN REPETIDA AQUI
            if not (self.num>=len(self.textos)-1):
                textoEntradaSize=len(self.textos[self.num])
            else:
                self.num = len(self.textos)-1 #self.MAX_DIALOG-1
                textoEntradaSize=len(self.textos[self.num])

            if self.i<textoEntradaSize:
                self.aux+=self.textos[self.num][self.i]
                self.i+=1
            else:
                self.finDialogo=True
        
            self.blit_text(win, self.pos_texto, self.PIXEL_TEXT)
    



    def blit_text(self, surface, pos, font, color=pygame.Color('white')):
        words = [word.split(' ') for word in self.aux.splitlines()]
        space = font.size(' ')[0]
        max_width = surface.get_width()-75
        max_height = surface.get_height()-75
        x, y = pos
        for line in words:
            for word in line:
                word_surface = font.render(word, 0, color)
                word_width, word_height = word_surface.get_size()
                if x + word_width >= max_width:
                    x = pos[0]
                    y += word_height
                surface.blit(word_surface, (x, y))
                x += word_width + space
            x = pos[0]
            y += word_height  
