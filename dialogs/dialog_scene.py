import pygame
from escena import Escena
from levels.level import Level1, Level2, Level3
import os



def divide_imagen(image, rect):
    subimage = image.subsurface(rect)
    return subimage, rect


class Dialogos(Escena):
	def __init__(self, director, comander, dialogos, inicio, sig):
		Escena.__init__(self, director)
		w=director.win.get_width()
		h=director.win.get_height()
		self.finDialogo=False
		self.chema= pygame.transform.scale(pygame.image.load(os.path.join("Assets","pantalla dialogos chema 2.png")),(w, h)).convert_alpha()
		self.olen= pygame.transform.scale(pygame.image.load(os.path.join("Assets","pantalla dialogos olen.png")),(w, h)).convert_alpha()

		# Cousas de falar
		self.chema_falar = pygame.image.load(os.path.join("Assets/dialogos","chema_falar.png")).convert_alpha()
		self.olen_falar = pygame.image.load(os.path.join("Assets/dialogos","olen_falar.png")).convert_alpha()

		# Cousas do parallax (que agora realmente so e unha animacion)
		self.tam_bg = (544, 320) 		 # 272,160
		self.tam_layer1 = (426, 284)	 # 213,142
		self.tam_layer2 = (544, 300)	 # 272,150
		self.tam_layer3 = (544, 208)	 # 272,104
		self.p_bg = pygame.transform.scale(pygame.image.load(os.path.join("Assets/dialogos/parallax-industrial-pack","bg.png")).convert_alpha(), self.tam_bg)
		self.p_layer1 = pygame.transform.scale(pygame.image.load(os.path.join("Assets/dialogos/parallax-industrial-pack","layer1.png")).convert_alpha(), self.tam_layer1)
		self.p_layer2 = pygame.transform.scale(pygame.image.load(os.path.join("Assets/dialogos/parallax-industrial-pack","layer2.png")).convert_alpha(), self.tam_layer2)
		self.p_layer3 = pygame.transform.scale(pygame.image.load(os.path.join("Assets/dialogos/parallax-industrial-pack","layer3.png")).convert_alpha(), self.tam_layer3)

		self.tiras_layer1 = []
		self.tiras_layer2 = []
		self.tiras_layer3 = []

		self.pos_bg = (380,50)
		self.pos_layer1 = (self.pos_bg[0], self.pos_bg[1]+36)
		self.pos_layer2 = (self.pos_bg[0], self.pos_bg[1]+20)
		self.pos_layer3 = (self.pos_bg[0], self.pos_bg[1]+112)

		self.despl = 0 	# Move o layer 1
		self.despl2 = 0 # Move o layer 2
		self.despl3 = 0 # Move o layer 3
		self.counter_draw = 0

		for x in range(0, self.p_layer1.get_width()):
			subimage, rect = divide_imagen(self.p_layer1, pygame.Rect((x,0) ,(1,self.tam_layer1[1]) ))
			self.tiras_layer1.append(subimage)
		for x in range(0, self.p_layer2.get_width()):
			subimage, rect = divide_imagen(self.p_layer2, pygame.Rect((x,0) ,(1,self.tam_layer2[1]) ))
			self.tiras_layer2.append(subimage)
		for x in range(0, self.p_layer3.get_width()):
			subimage, rect = divide_imagen(self.p_layer3, pygame.Rect((x,0) ,(1,self.tam_layer3[1]) ))
			self.tiras_layer3.append(subimage)

		pygame.mixer.Sound("Sounds/industrial.ogg").play()

		self.dialogos=dialogos
		self.MAX_DIALOG=len(self.dialogos)
		self.num=0 
		self.fondo=inicio #para saber quen empeza a falar (logo alternanse contestando un ao outro)
		self.aux=""
		self.i=0
		self.comander = comander
		self.sig=sig #siguiente nivel del juego que se tiene que cargar despues de esta escena

	def update(self, *args):
		pass
	
	def eventos(self, lista_eventos):
		for event in lista_eventos:
			if event.type == pygame.QUIT:
				self.director.salirPrograma()
			if self.num==self.MAX_DIALOG-1 and event.type == pygame.MOUSEBUTTONUP and event.button == 1:
				#pygame.mixer.stop()
				from dialogs.endlevel_scene import EndLevelScreen
				#escena que lanza el nivel "sig" del juego
				if self.sig==1:
					escena = Level1(self.director, self.comander)
				elif self.sig==2: # Esto vai de 2 dialogo (pre-seleccion) a seleccion (nivel 2)
					escena = Level2(self.director, self.comander)
				elif self.sig == 3:
					escena = Level3(self.director, self.comander)
				elif self.sig == 4:
					return
				elif self.sig == 8:	# Esto vai de intro a 1 dialogo (pre-seleccion)
					dialogo = ["Esto seria o dialogo do nivel 1 (pre-seleccion)", "Con total seguridade"]
					escena = Dialogos(self.director, self.comander, dialogo, "o", 9)
				elif self.sig == 9: # Esto vai de 1 dialogo (pre-seleccion) a seleccion (nivel 1)
					from selection import Seleccion
					escena = Seleccion(self.director, 1)
				elif self.sig == 10: # Esto vai de 2 dialogo (pre-seleccion) a seleccion (nivel 2)
					from selection import Seleccion
					escena = Seleccion(self.director, 2)
				elif self.sig == 11: # Esto vai de 3 dialogo (pre-seleccion) a seleccion (nivel 3)
					from selection import Seleccion
					escena = Seleccion(self.director, 3)
				#escena = EndLevelScreen(self.director, self.comander, 1, True)
				self.director.cambiarEscena(escena)
			if event.type == pygame.MOUSEBUTTONUP and event.button == 1 and self.finDialogo==True:
				self.num+=1
				if self.fondo=="c":
					self.fondo="o"
				else:
					self.fondo="c"
				self.i=0
				self.aux=""
				self.finDialogo=False

	def dibujar(self, win):
		letra_px = 'Assets/fonts/Avenixel-Regular.ttf'
		fuente = pygame.font.Font(letra_px, int(27))
		win.fill((0,0,0))
		win.blit(self.p_bg, self.pos_bg)
		channel2 = pygame.mixer.Channel(2)
		channel2.set_volume(0.3)
		channel3 = pygame.mixer.Channel(3)
		channel3.set_volume(0.3)
		chema_talk = pygame.mixer.Sound("Sounds/Talk1.ogg")
		olen_talk = pygame.mixer.Sound("Sounds/Talk2.ogg")

		# pintar as tiras
		if self.counter_draw == 3:
			self.despl += 1
			self.despl2 += 2
			self.despl3 += 4
			self.counter_draw = 0
		else:
			self.counter_draw += 1
		#depl_local = self.despl % 272
		for i1 in range(len(self.tiras_layer1)):
			win.blit(self.tiras_layer1[i1], (self.pos_layer1[0] + ((i1 + self.despl) % 544), self.pos_layer1[1]))
		for i2 in range(len(self.tiras_layer2)):
			win.blit(self.tiras_layer2[i2], (self.pos_layer2[0] + ((i2 + self.despl2) % 544), self.pos_layer2[1]))
		for i3 in range(len(self.tiras_layer3)):
			win.blit(self.tiras_layer3[i3], (self.pos_layer3[0] + ((i3 + self.despl3) % 544), self.pos_layer3[1]))

		#win.blit(self.p_layer1, self.pos_layer1)
		#win.blit(self.p_layer2, self.pos_layer2)
		#win.blit(self.p_layer3, self.pos_layer3)

		#if(self.fondo == "c"):
		#	win.blit(self.chema, (0,0))
		#else:
		#	win.blit(self.olen, (0,0))
		if(self.fondo == "c"):
			win.blit(self.chema_falar, (40,450))
			channel2.play(chema_talk)
		else:
			win.blit(self.olen_falar, (40,450))
			channel3.play(olen_talk)


		if not (self.num>=self.MAX_DIALOG-1):
			textoEntradaSize=len(self.dialogos[self.num])	
		else:
			self.num = self.MAX_DIALOG-1
			textoEntradaSize=len(self.dialogos[self.num])

		if self.i<textoEntradaSize:
			self.aux+=self.dialogos[self.num][self.i]
			self.i+=1
		else:
			self.finDialogo=True
			chema_talk.stop()
			olen_talk.stop()

		self.blit_text(win, (330, 505), fuente)
		pygame.mixer.stop()
		pygame.mixer.music.load("Sounds/background.ogg")
		pygame.mixer.music.set_volume(0.2)
		pygame.mixer.music.play(-1)


	def blit_text(self, surface, pos, font, color=pygame.Color('white')):
		words = [word.split(' ') for word in self.aux.splitlines()]  
		space = font.size(' ')[0]  
		max_width = surface.get_width()-75
		max_height = surface.get_height()-75
		x, y = pos
		for line in words:
			for word in line:
				word_surface = font.render(word, 0, color)
				word_width, word_height = word_surface.get_size()
				if x + word_width >= max_width:
					x = pos[0]  
					y += word_height  
				surface.blit(word_surface, (x, y))
				x += word_width + space
			x = pos[0]  
			y += word_height  


			
